import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiplicationTable(),
    );
  }
}

class MultiplicationTable extends StatelessWidget {
  List<List<int>> list = List<List<int>>.generate(
      11, (index) => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

  generate() {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: list.map((row) {
            return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: row.map((cont) {
                  if (list.indexOf(row) <= cont && list.indexOf(row) != 0) {
                    return Container(
                      alignment: Alignment.center,
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                        border: Border.all(),
                        color: list.indexOf(row) == cont
                            ? Colors.yellow
                            : Colors.white,
                      ),
                      child: Text(
                          ('${list.indexOf(row) != 0 ? cont * list.indexOf(row) : cont}')),
                    );
                  } else if (list.indexOf(row) == 0) {
                    return Container(
                      alignment: Alignment.center,
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                        border: Border.all(),
                        color: Colors.greenAccent,
                      ),
                      child: Text(('${cont}')),
                    );
                  } else {
                    return Container(
                      alignment: Alignment.center,
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                        border: Border.all(),
                        color: cont != 0
                            ? Colors.yellowAccent[100]
                            : Colors.greenAccent,
                      ),
                      child: cont != 0
                          ? Text(
                              ('${list.indexOf(row) != 0 ? cont * list.indexOf(row) : cont}'))
                          : Text('${list.indexOf(row)}'),
                    );
                  }
                }).toList());
          }).toList(),
        ),
      ),
    );
  }
}
